﻿namespace NoteApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addNoteButton = new System.Windows.Forms.Button();
            this.editNoteButton = new System.Windows.Forms.Button();
            this.deleteNoteButton = new System.Windows.Forms.Button();
            this.synchroniseNoteButton = new System.Windows.Forms.Button();
            this.notesListBox = new System.Windows.Forms.ListBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addNoteButton
            // 
            this.addNoteButton.Location = new System.Drawing.Point(681, 12);
            this.addNoteButton.Name = "addNoteButton";
            this.addNoteButton.Size = new System.Drawing.Size(107, 64);
            this.addNoteButton.TabIndex = 0;
            this.addNoteButton.Text = "Add Note";
            this.addNoteButton.UseVisualStyleBackColor = true;
            // 
            // editNoteButton
            // 
            this.editNoteButton.Location = new System.Drawing.Point(681, 154);
            this.editNoteButton.Name = "editNoteButton";
            this.editNoteButton.Size = new System.Drawing.Size(107, 68);
            this.editNoteButton.TabIndex = 1;
            this.editNoteButton.Text = "Edit Note";
            this.editNoteButton.UseVisualStyleBackColor = true;
            // 
            // deleteNoteButton
            // 
            this.deleteNoteButton.Location = new System.Drawing.Point(681, 82);
            this.deleteNoteButton.Name = "deleteNoteButton";
            this.deleteNoteButton.Size = new System.Drawing.Size(107, 66);
            this.deleteNoteButton.TabIndex = 2;
            this.deleteNoteButton.Text = "Delete Note";
            this.deleteNoteButton.UseVisualStyleBackColor = true;
            // 
            // synchroniseNoteButton
            // 
            this.synchroniseNoteButton.Location = new System.Drawing.Point(681, 228);
            this.synchroniseNoteButton.Name = "synchroniseNoteButton";
            this.synchroniseNoteButton.Size = new System.Drawing.Size(107, 70);
            this.synchroniseNoteButton.TabIndex = 3;
            this.synchroniseNoteButton.Text = "Synchronise with Database";
            this.synchroniseNoteButton.UseVisualStyleBackColor = true;
            this.synchroniseNoteButton.Click += new System.EventHandler(this.synchroniseNoteButton_Click);
            // 
            // notesListBox
            // 
            this.notesListBox.FormattingEnabled = true;
            this.notesListBox.ItemHeight = 16;
            this.notesListBox.Location = new System.Drawing.Point(12, 12);
            this.notesListBox.Name = "notesListBox";
            this.notesListBox.Size = new System.Drawing.Size(195, 420);
            this.notesListBox.TabIndex = 4;
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(681, 370);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(107, 68);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.notesListBox);
            this.Controls.Add(this.synchroniseNoteButton);
            this.Controls.Add(this.deleteNoteButton);
            this.Controls.Add(this.editNoteButton);
            this.Controls.Add(this.addNoteButton);
            this.Name = "Main";
            this.Text = "Note App";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addNoteButton;
        private System.Windows.Forms.Button editNoteButton;
        private System.Windows.Forms.Button deleteNoteButton;
        private System.Windows.Forms.Button synchroniseNoteButton;
        private System.Windows.Forms.ListBox notesListBox;
        private System.Windows.Forms.Button exitButton;
    }
}

