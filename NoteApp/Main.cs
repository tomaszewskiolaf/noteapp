﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteApp
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void synchroniseNoteButton_Click(object sender, EventArgs e)
        {
            var databaseWindow = new DatabaseWindow();
            databaseWindow.Show();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
