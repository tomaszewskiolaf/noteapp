﻿namespace NoteApp
{
    partial class DatabaseWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.databaseTextbox = new System.Windows.Forms.TextBox();
            this.loginTextbox = new System.Windows.Forms.TextBox();
            this.loginLabel = new System.Windows.Forms.Label();
            this.passwordTextbox = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.windowsAuthenticationCheckbox = new System.Windows.Forms.CheckBox();
            this.authenticationGroupBox = new System.Windows.Forms.GroupBox();
            this.authenticationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(62, 209);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // databaseLabel
            // 
            this.databaseLabel.AutoSize = true;
            this.databaseLabel.Location = new System.Drawing.Point(59, 18);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(69, 17);
            this.databaseLabel.TabIndex = 1;
            this.databaseLabel.Text = "Database";
            // 
            // databaseTextbox
            // 
            this.databaseTextbox.Location = new System.Drawing.Point(62, 38);
            this.databaseTextbox.Name = "databaseTextbox";
            this.databaseTextbox.Size = new System.Drawing.Size(100, 22);
            this.databaseTextbox.TabIndex = 2;
            // 
            // loginTextbox
            // 
            this.loginTextbox.Location = new System.Drawing.Point(62, 88);
            this.loginTextbox.Name = "loginTextbox";
            this.loginTextbox.Size = new System.Drawing.Size(100, 22);
            this.loginTextbox.TabIndex = 4;
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Location = new System.Drawing.Point(59, 68);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(43, 17);
            this.loginLabel.TabIndex = 3;
            this.loginLabel.Text = "Login";
            // 
            // passwordTextbox
            // 
            this.passwordTextbox.Location = new System.Drawing.Point(62, 141);
            this.passwordTextbox.Name = "passwordTextbox";
            this.passwordTextbox.Size = new System.Drawing.Size(100, 22);
            this.passwordTextbox.TabIndex = 6;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(59, 121);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(69, 17);
            this.passwordLabel.TabIndex = 5;
            this.passwordLabel.Text = "Password";
            // 
            // windowsAuthenticationCheckbox
            // 
            this.windowsAuthenticationCheckbox.AutoSize = true;
            this.windowsAuthenticationCheckbox.Location = new System.Drawing.Point(63, 170);
            this.windowsAuthenticationCheckbox.Name = "windowsAuthenticationCheckbox";
            this.windowsAuthenticationCheckbox.Size = new System.Drawing.Size(179, 21);
            this.windowsAuthenticationCheckbox.TabIndex = 7;
            this.windowsAuthenticationCheckbox.Text = "Windows authentication";
            this.windowsAuthenticationCheckbox.UseVisualStyleBackColor = true;
            // 
            // authenticationGroupBox
            // 
            this.authenticationGroupBox.Controls.Add(this.databaseLabel);
            this.authenticationGroupBox.Controls.Add(this.windowsAuthenticationCheckbox);
            this.authenticationGroupBox.Controls.Add(this.connectButton);
            this.authenticationGroupBox.Controls.Add(this.passwordTextbox);
            this.authenticationGroupBox.Controls.Add(this.databaseTextbox);
            this.authenticationGroupBox.Controls.Add(this.passwordLabel);
            this.authenticationGroupBox.Controls.Add(this.loginLabel);
            this.authenticationGroupBox.Controls.Add(this.loginTextbox);
            this.authenticationGroupBox.Location = new System.Drawing.Point(12, 12);
            this.authenticationGroupBox.Name = "authenticationGroupBox";
            this.authenticationGroupBox.Size = new System.Drawing.Size(269, 253);
            this.authenticationGroupBox.TabIndex = 9;
            this.authenticationGroupBox.TabStop = false;
            this.authenticationGroupBox.Text = "Log in";
            // 
            // DatabaseWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 277);
            this.Controls.Add(this.authenticationGroupBox);
            this.Name = "DatabaseWindow";
            this.Text = "DatabaseWindow";
            this.authenticationGroupBox.ResumeLayout(false);
            this.authenticationGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.TextBox databaseTextbox;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.CheckBox windowsAuthenticationCheckbox;
        private System.Windows.Forms.GroupBox authenticationGroupBox;
        public System.Windows.Forms.TextBox loginTextbox;
        public System.Windows.Forms.TextBox passwordTextbox;
    }
}