﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NoteApp
{
    public partial class DatabaseWindow : Form
    {
        public DatabaseWindow()
        {
            InitializeComponent();
        }

        //private string GetConnectionString()
        //{
           
        //        // To avoid storing the connection string in your code,
        //    // you can retrieve it from a configuration file.
        //    return $"Server=(local);Integrated Security={windowsAuthenticationCheckbox.Checked};" +
        //        $"Initial Catalog={databaseTextbox.Text}";
        //}

        private void connectButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Login = loginTextbox.Text;
            Properties.Settings.Default.Password = passwordTextbox.Text;
            Properties.Settings.Default.Database = databaseTextbox.Text;
            Properties.Settings.Default.IsWindowsAuthentication = windowsAuthenticationCheckbox.Checked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Upgrade();
            Properties.Settings.Default.Reload();
            //string connectionString = GetConnectionString();
            //string queryString = "SELECT * FROM Notes";

            //using (SqlConnection connection = new SqlConnection(
            //   connectionString))
            //{
            //    SqlCommand command = new SqlCommand(queryString, connection);

            //    try
            //    {
            //        command.Connection.Open();
            //        //command.ExecuteNonQuery();

            //        var reader = command.ExecuteReader();
            //        while (reader.Read())
            //        {
            //            MessageBox.Show($"{reader["Title"].ToString()}");
            //        }
            //        command.Connection.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString());
            //    }
            //}
        }
    }
}
